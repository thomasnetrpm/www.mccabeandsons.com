<?php if (get_field('show_site_intro')): ?>
	<div class="site-intro">
	  	<div class="container">
	  		<div class="si-content">
			  	<?php if(get_field('si_heading')):?>
			   	<h1 class="sis-title"><?php the_field('si_heading');?></h1>
				 	<?php else: ?>
				  	<h1 class="sis-title"><?php the_title(); ?></h1>
					<?php endif;?>
					
				</div>
	  	</div>
	  	<div class="si-slider">
        <?php $img = get_field('si_slider');if( $img ): ?>   
		<?php foreach( $img as $image ): ?>	
          <div class="si-item" style="background-image: url(<?php echo $image['url']; ?>);"></div>
          <?php endforeach; ?>    
           <?php endif; ?>
        </div> 
	</div>
<?php endif ?>