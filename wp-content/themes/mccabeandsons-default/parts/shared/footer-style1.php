<!--Site Footer Start-->
<footer class="site-footer footer-style2" role="contentinfo">
    <div class="container">
        
        <?php $logo = get_field('global_company_logo','option');?>
        <div class="sf-middle-logo-nav">
            <?php if( !empty($logo) ): ?>
                <div class="order-2"><a href="<?php bloginfo('url'); ?>" class="sf-logo">
                    <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['alt']; ?>">
                </a></div>
            <?php endif;?>
            <?php wp_nav_menu(array(
                'menu'            => 'Footer Left Menu',
                'container'       => 'ul',
                'menu_class' => 'sf-links',
            )); ?>
            <?php wp_nav_menu(array(
                'menu'            => 'Footer Right Menu',
                'container'       => 'ul',
                'menu_class' => 'sf-links order-3',
            )); ?>
        </div>

        <ul class="sf-contact-info">
        <?php if(get_field('global_company_name','option')):?>
            <!--<li class="sf-company"><?php the_field('global_company_name','option');?></li>-->
        <?php endif;?>
        <?php if(get_field('global_address','option')):?>
            <li class="sf-address"><?php the_field('global_address','option');?></li>
        <?php endif;?>
            <?php $string = get_field('global_phone_number','option');$string = preg_replace("/[^0-9]/", '', $string);?>
            <?php if ($string): ?>
            </ul>
            <ul class="sf-contact-info">
                <li class="sf-ph">Tel: <a href="tel:<?php echo $string;?>"><?php the_field('global_phone_number','option');?></a></li>
            <?php endif ?>                 

            <?php if (get_field('global_fax','option')): ?>
                <li class="sf-fax">Fax: <a href="javascript:void(0)" class="nonlink fax" tabindex="-1"><?php the_field('global_fax','option');?></a></li>
            <?php endif;?>

            <?php if(get_field('global_email','option')):?>
            <li>Em: <a href="mailto:<?php the_field('global_email','option');?>" class="sf-mail"><?php the_field('global_email','option');?></a></li>
            <?php endif;?>
        </ul>

         <?php $tlogo = get_field('global_thomas_logo_footer','option');?>
            <?php if( !empty($logo) ): ?>
            <div class="">
                <?php 
                    $link = get_field('thomas_link', 'option');
                    if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                <a href="<?php echo esc_url( $link_url ); ?>" class="sf-thomas-logo">
                <img src="<?php echo $tlogo['url']; ?>" alt="<?php echo $tlogo['alt']; ?>" title="<?php echo $tlogo['alt']; ?>">
                </a>
                <?php endif; ?>
            </div>
        <?php endif;?>
    </div>

    <div class="footer-bootom sf-small">
        <div class="container">
            <p class="copyright">&copy; <?php echo date("Y"); ?> <a href="<?php bloginfo('url'); ?>"><?php bloginfo( 'name' ); ?></a>, All Rights Reserved&nbsp; | &nbsp;Site created by <a href="https://business.thomasnet.com/marketing-services" target="_blank" rel="noreferrer noopener">Thomas Marketing Services</a></p>
        </div>
    </div>
</footer>
<!--Site Footer End-->