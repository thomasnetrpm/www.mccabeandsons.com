<header class="site-header header4">
	<div class="top-line">
		<div class="container">
			<div class="d-flex align-items-center justify-content-between">
				<!-- Your site title as branding in the menu -->
				<?php $logo = get_field('global_company_logo','option'); ?>
				<?php if ( !$logo && ! has_custom_logo() ) { ?>

					<?php if ( is_front_page() && is_home() ) : ?>

						<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

					<?php else : ?>

						<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

					<?php endif; ?>


				<?php } else {
					if( !empty($logo) ): ?>
	                    <a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['alt']; ?>"></a>
	                <?php else: 
	                	the_custom_logo();
	                 endif;
				} ?><!-- end custom logo -->

				<?php
		        $tel_number = get_field('global_phone_number','option');
		        $unformatted_tel_number = preg_replace("/[^0-9]/", '', $tel_number);?>
		        <?php if ($tel_number): ?>
		              <span class="sh-ph d-none d-md-inline-block"><a class="cms_phone" href="tel:<?php echo $unformatted_tel_number;?>" aria-label="Phone Number" title="<?php echo $unformatted_tel_number;?>"><i class="fa fa-phone" aria-hidden="true"></i> <span><?php echo $tel_number;?></span></a></span>
		        <?php endif ?>

		        <?php
		        $email_address = get_field('global_email', 'option');
		        if ($email_address): ?>
		            <span class="sh-email d-none d-md-inline-block"><a class="cms_email" href="mailto:<?php echo strtolower($email_address); ?>" title="Email Us"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span><?php echo $email_address; ?></span></a></span>
		        <?php endif ?>

				<?php 
				$link = get_field('request_quote_link', 'option');
				if( $link ): 
				    $link_url = $link['url'];
				    $link_title = $link['title'];
				    $link_target = $link['target'] ? $link['target'] : '_self';
				    ?>
				    <a class="button btn btn-primary d-none d-md-inline-block" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="header-inner sh-sticky-wrap">

		<nav class="navbar navbar-expand-md navbar-dark bg-primary">

			<div class="container">

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<?php
		        $tel_number = get_field('global_phone_number','option');
		        $unformatted_tel_number = preg_replace("/[^0-9]/", '', $tel_number);?>
		        <?php if ($tel_number): ?>
		              <span class="sh-ph d-md-none"><a class="cms_phone" href="tel:<?php echo $unformatted_tel_number;?>" aria-label="Phone Number" title="<?php echo $unformatted_tel_number;?>"><i class="fa fa-phone" aria-hidden="true"></i> <span><?php echo $tel_number;?></span></a></span>
		        <?php endif ?>

		        <?php
		        $email_address = get_field('global_email', 'option');
		        if ($email_address): ?>
		            <span class="sh-email d-md-none"><a class="cms_email" href="mailto:<?php echo strtolower($email_address); ?>" title="Email Us"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span><?php echo $email_address; ?></span></a></span>
		        <?php endif ?>

				<a href="#search" class="search-form-tigger order-md-2"  data-toggle="search-form"><i class="fa fa-search" aria-hidden="true"></i></a>

				<?php if( $link ): ?>
				<a class="button btn btn-secondary d-md-none px-4 m-0" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">RFQ</a>
				<?php endif;?>


				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav ml-auto main-menu',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
			</div><!-- .container -->
		</nav>
	</div>
</header>